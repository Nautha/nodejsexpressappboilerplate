const {helloRouter} = require('./hello');

module.exports = {
    routes: [
        helloRouter
    ]
};