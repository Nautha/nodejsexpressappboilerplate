const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

const {routes} = require('./routes');
const {errorHandler} = require('./middleware/error');
const {ServiceLocator, registerServices} = require('./serviceLocator/ServiceLocator');
const {initialization} = require('./initialization');

(async function() {
    const serviceLocator = new ServiceLocator();
    registerServices(serviceLocator);

    await serviceLocator.get('DatabaseController')._connect();
    await initialization(serviceLocator);

    app.locals.serviceLocator = serviceLocator;

    app.use(bodyParser.json());
    app.use(cors());
    routes.forEach(route => app.use(route));

    app.use(errorHandler);

    app.listen(8080, () => console.log("\x1b[32m%s\x1b[0m", 'Server started'));
})();