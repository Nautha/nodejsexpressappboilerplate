const mysql = require('mysql2/promise');

class DatabaseController {
    constructor() {
        this.host = process.env.DB_HOST;
        this.port = process.env.DB_PORT;
        this.user = process.env.DB_USER;
        this.password = process.env.DB_PASSWORD;
        this.database = process.env.DB_DATABASE;
    }

    async _connect() {
        this.connection = await mysql.createConnection({
            host: this.host,
            port: this.port,
            user: this.user,
            password: this.password,
            database: this.database
        });
    }

    async query(query, params = []) {
        try {
            if (query.indexOf(':') >= 0) {
                this.prepareStatement(query, params);
            } else {
                return await this._executePreparedStatement(query, params);
            }
        } catch (e) {
            throw e;
        }
    }

    async _executePreparedStatement(query, params = []) {
        try {
            const [rows, fields] = await this.connection.execute(query, params);
            return rows;
        } catch (e) {
            throw e;
        }
    }

    async prepareStatement(query, params) {
        const queryArray = query.split(' ');

        let newQuery = '';
        let newParams = [];

        queryArray.forEach(word => {
           if(word.indexOf(':') >= 0) {
                newQuery += "? ";
                newParams.push(params[word]);

           } else {
               newQuery += word + " ";
           }
        });

        return await this._executePreparedStatement(newQuery, newParams);
    }

    async doesTableExist() {
        try {
            return await this.connection.query('SHOW TABLES LIKE \'patients\'');
        } catch (e) {
            throw (e);
        }
    }

    async createTable(query) {
        try {
            return await this.connection.query(query);
        } catch (e) {
            throw (e);
        }
    }
}

module.exports = {DatabaseController};